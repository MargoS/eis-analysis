% Written by MStraka 3/2018.

%Loads each .mat data file. Uses the CategorizeData function to categorizes the data into groups based on magnitude and phase of impedance.
%Outputs the SummaryData struct. SummaryData is an {a x t} cell, where a is the array ID and t is the time. Each cell index includes data and group
%information

%Inputs to include times (the date of data collection as labeled by folder
%name), arrays (the array ID, as labeled by the folder name), the
%channels for the array, and the starting directory. For eg:
% times={ 'Pre-Implant','Post-Implant (1h)','2 W', '3 W', '4 W', '5 W', '6 W', '7 W', '8 W', '9 W', '10 W', '11 W', '12 W'}; %time of data collection
% arrays={'iUA-1_', 'iUA-3_','iUA-7_','iUA-10_','iUA-11_','yUA-5_','yUA-8_','yUA-9_','yUA-11_','yUA-12_','yUA-13_'}; %the array ID
% ch=1:16;
% dirName='Matfiles'; This is the name of the main directory
%Need to start the function in the directory with all of the Matlab files
%of the data for this to correctly sort through all data

function [SummaryData]=SummarizeEISData(times, arrays, ch, dirName)
fileDir=dir(dirName); %need to start above the directory with the matlab files
for i=1:length(fileDir)
    FileNames{i}=fileDir(i).name;
end

for i=1:length(arrays) %for each array
    arrayind=strfind(FileNames, arrays{i}); %find the desired array. This keeps the times organized consistently in SummaryData
    for t=1:length(times) %for each timepoint
        timeind= strfind(FileNames, times{t}); %match the order with the variable times. This keeps the times organized consistently in SummaryData
        AllChOneMoment{1}=[]; %initialize. This temporary variable combines data for all channels in one array at one timepoint
        for c=1:length(ch) %for each channel
            chind=strfind(FileNames, ['Ch' num2str(ch(c)) '.mat']); %find the desired channel. This keeps channels organized 1:16
            for j=1:length(fileDir) %for each data file
                if ~isempty(timeind{j}) && ~isempty(arrayind{j}) && ~isempty(chind{j})
                    datafile=load([dirName, '/', FileNames{j}]); %load the data
                    
                    AllChOneMoment{ch(c)}{1}=datafile.data.data(:,3); %frequency information
                    AllChOneMoment{ch(c)}{2}=datafile.data.data(:,7); %impedance magnitude
                    AllChOneMoment{ch(c)}{3}=datafile.data.data(:,8); %impedance phase
                    if(length(datafile.data.data(:,3))>10) %if data is sufficient in length
                        if i<=5 %if an iUA array
                            arrayType='IrOx';
                        else %if a pt yArray
                            arrayType='Pt';
                        end
                        [group]=CategorizeData(arrayType,datafile.data.data);
                        AllChOneMoment{ch(c)}{4}=group;
                        clear datafile
                    end
                end
            end
        end
        SummaryData{i,t}.Data=AllChOneMoment; %summarizes the impedance data
        SummaryData{i,t}.ArrayTime={arrays{i}, times{t}}; %specifies the array ID and 
        clear AllChOneMoment
    end
end
save SummaryData