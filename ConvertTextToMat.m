% Written by MStraka 3/2018. 

%This function converts the text files to mat files. Can ignore if your
%data is already converted. Depending on organization of your data and the naming convention, will
%likely need to tailor this.

%Be sure to start in the directory with the data, called 'mainDir'. For eg,
%mainDir='EIS';

function ConvertTextToMat(mainDir)
timeDir=dir(mainDir);
for i=1:length(timeDir)-3 %for each main folder- week time
    timeFileName{i}=timeDir(i+3).name; %List of all the folders (Weeks). first three aren't folders
    trodeDir{i}=dir([mainDir, '/', timeFileName{i}]); %List all subfolders
    for j=1:length(trodeDir{1,i})-2  %for each type (iUA or uA)
        trodeName{i,j}=trodeDir{i}(j+2).name;
        if strcmp(trodeName{i,j}, '.DS_Store') %ignore these, not data
        else
            dataDir{i,j}=dir([mainDir,'/' ,timeFileName{i}, '/' trodeName{i,j}]);
            for k=1:length(dataDir{i,j})-2
                dataName{i,j,k}=  dataDir{i,j}(k+2).name;
                if strcmp( dataName{i,j,k}, '.DS_Store')
                else
                    data=importdata([mainDir, '/', timeFileName{i} '/' trodeName{i,j} '/' dataName{i,j,k}],'\t', 59);
                    %sometimes need more rows for the headers. 
                    if (isfield(data, 'data'))
                    else
                        for l=60:100
                            data=importdata([mainDir, '/', timeFileName{i} '/' trodeName{i,j} '/' dataName{i,j,k}],'\t', l);
                            if (isfield(data, 'data'))
                                break
                            end
                            
                        end
                    end
                    
                    %Process and save
                    if (isfield(data, 'data')) %check to see if the struct has data
                        if isnan( data.data(1))
                            data.data(:,1)=[];
                            data.colheaders(1)=[];
                        end
                        save(char(['Time' timeFileName{i} '_Array' trodeName{i,j} '_Ch' dataName{i,j,k}(1:end-4) '.mat']), 'data')
                    else
                        disp(['Not Converted: EIS_' timeFileName{i} '_' trodeName{i,j} '_' dataName{i,j,k}(1:end-4) '.mat'])
                    end
                    clear data
                end
            end
        end
    end
end

