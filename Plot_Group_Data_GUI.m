function varargout = Plot_Group_Data_GUI(varargin)
% PLOT_GROUP_DATA_GUI MATLAB code for Plot_Group_Data_GUI.fig
%      PLOT_GROUP_DATA_GUI, by itself, creates a new PLOT_GROUP_DATA_GUI or raises the existing
%      singleton*.
%
%      H = PLOT_GROUP_DATA_GUI returns the handle to a new PLOT_GROUP_DATA_GUI or the handle to
%      the existing singleton*.
%
%      PLOT_GROUP_DATA_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOT_GROUP_DATA_GUI.M with the given input arguments.
%
%      PLOT_GROUP_DATA_GUI('Property','Value',...) creates a new PLOT_GROUP_DATA_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Plot_Group_Data_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Plot_Group_Data_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Plot_Group_Data_GUI

% Last Modified by GUIDE v2.5 08-Feb-2018 17:06:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Plot_Group_Data_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @Plot_Group_Data_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Plot_Group_Data_GUI is made visible.
function Plot_Group_Data_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Plot_Group_Data_GUI (see VARARGIN)

% Choose default command line output for Plot_Group_Data_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Plot_Group_Data_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Plot_Group_Data_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in selectShowGroup.
function selectShowGroup_Callback(hObject, eventdata, handles)
% hObject    handle to selectShowGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectShowGroup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectShowGroup

handles.GroupShow=get(hObject,'Value');
updatePlotData(hObject, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function selectShowGroup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectShowGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.GroupOptions={'All Groups', 'Group 1', 'Group 2','Group 3', 'Outliers'};
handles.GroupShow=1;
set(hObject, 'String', handles.GroupOptions);
set(hObject, 'Value', handles.GroupShow);
guidata(hObject, handles);

% --- Executes on selection change in chooseArray.
function chooseArray_Callback(hObject, eventdata, handles)
% hObject    handle to chooseArray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns chooseArray contents as cell array
%        contents{get(hObject,'Value')} returns selected item from chooseArray
handles.showArrayN=get(hObject,'Value');
handles.showTimeN=1;
updatePlotData(hObject, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function chooseArray_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chooseArray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.ArrayOptions={' '};%'iUA-1', 'iUA-3','iUA-7','iUA-10','iUA-11','UA-5','UA-8','UA-9','UA-11','UA-12','UA-13'};
handles.showArrayN=1;
set(hObject, 'String', handles.ArrayOptions);
set(hObject, 'Value', handles.showArrayN);
guidata(hObject, handles);

% --- Executes on button press in AddGrp1.
function AddGrp1_Callback(hObject, eventdata, handles)
% hObject    handle to AddGrp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[x,y] = ginput(1);
newGrp=1;
[closestCh] = findClosestPt(x,y, handles);
handles.data{handles.showArrayN,handles.showTimeN}.Data{closestCh}{1,4}=newGrp;
updatePlotData(hObject, handles);
guidata(hObject, handles);


% --- Executes on button press in AddGrp2.
function AddGrp2_Callback(hObject, eventdata, handles)
% hObject    handle to AddGrp2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[x,y] = ginput(1);
newGrp=2;
[closestCh] = findClosestPt(x,y, handles);
handles.data{handles.showArrayN,handles.showTimeN}.Data{closestCh}{1,4}=newGrp;
updatePlotData(hObject, handles);
guidata(hObject, handles);

% --- Executes on button press in AddGrp3.
function AddGrp3_Callback(hObject, eventdata, handles)
% hObject    handle to AddGrp3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[x,y] = ginput(1);
newGrp=3;
[closestCh] = findClosestPt(x,y, handles);
handles.data{handles.showArrayN,handles.showTimeN}.Data{closestCh}{1,4}=newGrp;
updatePlotData(hObject, handles);
guidata(hObject, handles);

% --- Executes on button press in selectOutlier.
function selectOutlier_Callback(hObject, eventdata, handles)
% hObject    handle to selectOutlier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[x,y] = ginput(1);
newGrp=4;
[closestCh] = findClosestPt(x,y, handles);
handles.data{handles.showArrayN,handles.showTimeN}.Data{closestCh}{1,4}=newGrp;
updatePlotData(hObject, handles);
guidata(hObject, handles);

% --- Executes on button press in loadPrevData.
function loadPrevData_Callback(hObject, eventdata, handles)
% hObject    handle to loadPrevData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.showTimeN == 1
else
    handles.showTimeN=handles.showTimeN-1;
    updatePlotData(hObject, handles);
end
guidata(hObject, handles);

% --- Executes on button press in loadNextData.
function loadNextData_Callback(hObject, eventdata, handles)
% hObject    handle to loadNextData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SummaryData=handles.data;
save([handles.savePN, handles.saveFN], 'SummaryData')

%if there is not more data for the Time, start with the next array
if handles.showTimeN == 13 || length(handles.data{handles.showArrayN,handles.showTimeN}.Data) == 1
    if handles.showArrayN == 11 && handles.showTimeN == 13 %state end if the last data point
        plot(handles.dataPlot, 1:1:100,'Color', [1 1 1]) %clear the plot
        handles.dataTitle=['No More Data! Save Now!'];
        load gong.mat;
        sound(y); 
    else
        handles.showArrayN=handles.showArrayN+1;
        handles.showTimeN=1;
    end
else
    handles.showTimeN=handles.showTimeN+1;
end

if length(handles.data{handles.showArrayN,handles.showTimeN}.Data) == 1
    handles.showArrayN=handles.showArrayN+1;
    handles.showTimeN=1;    
end

if handles.showArrayN == 12 %state end if the last data point
    plot(handles.dataPlot, 1:1:100,'Color', [1 1 1]) %clear the plot
    handles.dataTitle=['No More Data! Save Now!'];
    load gong.mat;
    sound(y);
else
    handles.chooseArray.Value=handles.showArrayN;
    updatePlotData(hObject, handles);
end

guidata(hObject, handles);


% --- Executes on button press in saveData.
function saveData_Callback(hObject, eventdata, handles)
% hObject    handle to saveData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% [FileName,PathName]  = uiputfile('*.mat');
SummaryData=handles.data;
save([handles.savePN, handles.saveFN], 'SummaryData')


% --- Executes on button press in loadData.
function loadData_Callback(hObject, eventdata, handles)
% hObject    handle to loadData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uigetfile('*.mat');
data=load([PathName, FileName]);
handles.data=data.SummaryData;
[FileName,PathName]  = uiputfile('*.mat','Select Name for Saving Changes');
handles.showArrayN=1;
handles.showTimeN=1;
handles.saveFN=FileName;
handles.savePN=PathName;
[n, ~]=size(handles.data);
for array=1:n
    handles.ArrayOptions{array}=handles.data{array,1}.ArrayTime{1};
end
handles.chooseArray.Value=handles.showArrayN;
handles.chooseArray.String=handles.ArrayOptions;
updatePlotData(hObject, handles);
guidata(hObject, handles);


function updatePlotData(hObject, handles)
groupShow=handles.GroupShow;
ArrayN=handles.showArrayN;
TimeN=handles.showTimeN;
data=handles.data;
legendTitles={'ch 1', 'ch 2', 'ch 3', 'ch 4', 'ch 5', 'ch 6', 'ch 7', 'ch 8', 'ch 9', 'ch 10', 'ch 11', 'ch 12', 'ch 13', 'ch 14', 'ch 15', 'ch 16'};
count=1;
legendInd=[];
if handles.plot_mag_rb.Value==1
    colInd=2;
else
    colInd=3;
end
    
if groupShow==1 % show all
    plot(handles.dataPlot, 1:1:100,'Color', [1 1 1]) %clear the plot
    for ch=1:16
        if length(data{ArrayN,TimeN}.Data{ch})>2
           if colInd==2
            loglog(handles.dataPlot, data{ArrayN,TimeN}.Data{ch}{1,1},data{ArrayN,TimeN}.Data{ch}{1,colInd})
           else
               plot(handles.dataPlot, log(data{ArrayN,TimeN}.Data{ch}{1,1}),data{ArrayN,TimeN}.Data{ch}{1,colInd})
               xticklabels('')
           end
            legendInd(count)=ch;
            count=count+1;
             hold(handles.dataPlot, 'on')
        end
    end
    hold(handles.dataPlot, 'off')
    axis(handles.dataPlot, 'tight')
else
    legendInd=subPlotGrps(groupShow-1, data{ArrayN,TimeN}, handles.dataPlot, colInd); 
end

xlabel('Frequency')
% ylabel('Magnitude')
if ~isempty(legendInd)
    legend(handles.dataPlot,legendTitles{legendInd}, 'Location', 'eastoutside')
else
    legend(handles.dataPlot,'No Data', 'Location', 'eastoutside')
end
title=['Array ', num2str(data{ArrayN,TimeN}.ArrayTime{1}),'  ', num2str(data{ArrayN,TimeN}.ArrayTime{2})];
set(handles.dataTitle,'String',title);
subPlotGrps(1, data{ArrayN,TimeN}, handles.plotGrp1,colInd);
subPlotGrps(2, data{ArrayN,TimeN}, handles.plotGrp2,colInd);
subPlotGrps(3, data{ArrayN,TimeN}, handles.plotGrp3,colInd);
subPlotGrps(4, data{ArrayN,TimeN}, handles.plotGrp4,colInd);

guidata(hObject, handles);

function [legendInd]=subPlotGrps(group, data, plotHandle,colInd)
count=1;
hold(plotHandle, 'off')
plot(plotHandle, 1:1:100,'Color', [1 1 1]) %clear the plot
legendInd=[];
for ch=1:16
    if length(data.Data{ch})>3
        if  data.Data{ch}{1,4} == group
            if colInd==2
           loglog(plotHandle, data.Data{ch}{1,1},data.Data{ch}{1,colInd})
           else
              plot(plotHandle, log(data.Data{ch}{1,1}),data.Data{ch}{1,colInd})
              
            end       
            
           hold(plotHandle, 'on')
           legendInd(count)=ch;
           count=count+1;
        end
    end
end
hold(plotHandle, 'off')
axis(plotHandle, 'tight')
xticklabels('')


function [chanClosest] = findClosestPt(x,y, handles)
ArrayN=handles.showArrayN;
TimeN=handles.showTimeN;
data=handles.data;
groupShow=handles.GroupShow;
if handles.plot_mag_rb.Value==1
    colInd=2;
else
      colInd=3;
end
 if length(data{ArrayN,TimeN}.Data{1})>1
     ch=1;
 else
     ch=2;
 end
 if colInd==2
     [~, xInd]=min(abs(data{ArrayN,TimeN}.Data{ch}{1,1}-x));  %find the nearest x-value = freq
 else
     [~, xInd]=min(abs(log(data{ArrayN,TimeN}.Data{ch}{1,1})-x));
 end
if groupShow == 1
    for ch=1:16 %arrange the magnitude at this index into an array
       if length(data{ArrayN,TimeN}.Data{ch})>3
        ypoints(ch)=data{ArrayN,TimeN}.Data{ch}{1,colInd}(xInd,1);
       end
    end
else
    for ch=1:16 %arrange the magnitude at this index into an array
        if length(data{ArrayN,TimeN}.Data{ch})>3
            if data{ArrayN,TimeN}.Data{ch}{1,4}==groupShow-1 %groups are adjusted by 1, since 1=all
                ypoints(ch)=data{ArrayN,TimeN}.Data{ch}{1,colInd}(xInd,1);
            end
        end
    end
end
[~, chanClosest]=min(abs(ypoints-y));


% --- Executes on button press in plot_mag_rb.
function plot_mag_rb_Callback(hObject, eventdata, handles)
% hObject    handle to plot_mag_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_mag_rb
rbVal=get(hObject,'Value');
if rbVal==1
    handles.plot_phase_rb.Value=0;
else
    handles.plot_phase_rb.Value=1;
end
updatePlotData(hObject, handles)
guidata(hObject, handles);

% --- Executes on button press in plot_phase_rb.
function plot_phase_rb_Callback(hObject, eventdata, handles)
% hObject    handle to plot_phase_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_phase_rb
rbVal=get(hObject,'Value');
if rbVal==1
    handles.plot_mag_rb.Value=0;
else
    handles.plot_mag_rb.Value=1;
end
updatePlotData(hObject, handles)
guidata(hObject, handles);
