# EIS analysis

%Written by Margo Straka 3/2018. For support please contact margo.straka@gmail.com

Code overview:

1. ConvertTextToMat -converts the txt files of all the data to .mat files for processing
Ignore this function if you have .mat files.  

2. SummarizeEISData- Loads each .mat data file, categorizes the data into groups based on magnitude and phase of impedance, and outputs
the SummaryData struct. Uses the function CatogorizeData to categorize into groups.
 
3. Plotting and Validating
Plot_Group_Data_GUI- visualization tool to plot and re-categorize groups. 
Load the SummaryData from (2) to see pre-manual sorts. Note that this GUI first wants to open the file, 
and then asks what name to save. It saves there each time 'Next' is selected