function [group]=CategorizeData(arrayType,data)
group=3; %default
if strcmp(arrayType,'IrOx')
    ind=find(data(:,3)<100);
    indLh=find(data(:,3)>1e4);
    indF=find(data(:,3)>5e4);
    if length(ind)>0
        meanInitMag=mean(data(ind,7)); %mean mag at f<100Hz
        meanInitPh=mean(data(ind,8)); %mean phase at f<100Hz
        meanFinalPh=mean(data(indF,8)); %mean phase at f>50kHz
        meanLHPh=mean(data(indLh,8)); %mean phase at f>10kHz
        if meanInitMag<8e6 && meanInitPh<-40 && meanInitPh>-80 && max(data(indF(end):ind(1),8))>-30 && abs(max(data(indF(end):ind(1),8))-meanInitPh)>25
            group=1;
        elseif meanInitMag>2e9
            group =4;
        elseif meanLHPh<-80
            group=2;
        elseif meanInitPh>-35 && meanFinalPh<-70 && max(diff(data(:,8)))>8
            group=2;
        end
    end
    
elseif strcmp(arrayType,'Pt')
    ind=find(data(:,3)<100);
    meanInitMag=mean(data(ind,7));
    if meanInitMag>1e8
        group=4;
    end
else
    disp(['Array Type Unknown, Assigned group 3'])
end